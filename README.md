# Entity Action

## Introduction

This module executes one-time actions based on user and entity events. Some use cases for this module are:

* Executing code for specific users, based on contextual data.
* Adding a new user to another entity's entity reference field upon registration or login (e.g. pending group registration).
* Delaying the execution of code during a user or entity event until some condition is met.

Everything this module does can be accomplished using Rules, the main benefit is it provides a very simple API and UI.

## INSTALLATION

Install the module as per: [https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)

## USAGE

### Processor creation

In order to execute an action when an event is triggered, you need to add a processor plugin to your custom module in src/Plugin/entity_action/EntityActionProcessor directory. This code will process each individual entity action.

```php

// custom_module/src/Plugin/entity_action/EntityActionProcessor/NewUserEntityActionProcessor.php

namespace Drupal\custom_module\Plugin\entity_action\NewUserEntityActionProcessor;

/**
 * @EntityActionProcessor(
 *   id = "custom_module_entity_insert_processor",
 *   name = @Translation("New User Processor"),
 *   action_type = Drupal\entity_action\Entity\EntityAction::ENTITY_ACTION_INSERT,
 *   entity_type = "user",
 *   entity_bundle = "user",
 * )
 */
class NewUserEntityActionProcessor extends EntityActionProcessorBase
{
    static public function process(EntityInterface $entity, EntityAction $entityAction, &$delete): void
    {
        try {
            $context = $entityAction->getContext();

            if (!($entity instanceof User)) {
                return;
            }
            if (!isset($context['email'])) {
                return;
            }
            if (!isset($context['node'])) {
                return;
            }
            if ($context['email'] !== $entity->getEmail()) {
                return;
            }

            $node = Drupal::service('entity.repository')->loadEntityByUuid('node', $context['node']);
            if ($node instanceof Node) {
                $node->get('field_members')->appendItem(['target_id' => $entity->id()]);
                $node->save();
                $delete = true;
            }
        }
        catch (Exception $e) {
            Drupal::logger('custom_module')->error($e->getMessage());
        }
    }
}
```

### Action creation

The next step is to create an action based on some kind of context. For example you may want to allow users to add a new user to a group when they register, based on an email address. You may have a page with a form that provides input for an email address, and executes this code during form submission.

```php
/* @var $entityActionManager EntityActionManagerService */
$entityActionManager = \Drupal::service('entity_action.manager');
$entityActionManager::createAction(
    EntityAction::ENTITY_ACTION_INSERT . ':' . $uuid,
    EntityAction::ENTITY_ACTION_INSERT,
    'user',
    'user',
    [
        'email' => $email,
        'node' => $uuid,
    ]
);
```

Now when a user registers, they will automatically be added to the field_members for that course by the entity_action module using the processor defined in your custom module.
