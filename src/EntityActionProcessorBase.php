<?php

namespace Drupal\entity_action;

use Drupal\Core\Plugin\PluginBase;

abstract class EntityActionProcessorBase extends PluginBase implements EntityActionProcessorInterface
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->pluginDefinition['name'];
    }

    /**
     * @return string
     */
    public function getActionType(): string
    {
        return $this->pluginDefinition['action_type'];
    }

    /**
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->pluginDefinition['entity_type'];
    }

    /**
     * @return string
     */
    public function getEntityBundle(): string
    {
        return $this->pluginDefinition['entity_bundle'];
    }
}
