<?php

namespace Drupal\entity_action\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;

/**
 * @ContentEntityType(
 *   id = "entity_action",
 *   label = @Translation("Entity Action"),
 *   base_table = "entity_action",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_action\Entity\EntityActionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "delete" = "Drupal\entity_action\Form\EntityActionDeleteForm",
 *     },
 *     "access" = "Drupal\entity_action\Entity\EntityActionAccessControlHandler",
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/entity-action/list",
 *     "canonical" = "/admin/config/system/entity-action/{entity_action}",
 *     "delete-form" = "/admin/config/system/entity-action/{entity_action}/delete",
 *   },
 * )
 */
class EntityAction extends ContentEntityBase implements ContentEntityInterface
{
    use EntityChangedTrait;

    const ENTITY_ACTION_INSERT = 'ENTITY_ACTION_INSERT';
    const ENTITY_ACTION_UPDATE = 'ENTITY_ACTION_UPDATE';
    const ENTITY_ACTION_DELETE = 'ENTITY_ACTION_DELETE';
    const ENTITY_ACTION_USER_LOGIN = 'ENTITY_ACTION_USER_LOGIN';
    const ENTITY_ACTION_USER_LOGOUT = 'ENTITY_ACTION_USER_LOGOUT';

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['name'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Name'))
            ->setSettings(['max_length' => 512, 'text_processing' => 0,]);

        $fields['action_type'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Action Type'))
            ->setSettings(['max_length' => 512, 'text_processing' => 0,]);

        $fields['entity_type'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Entity Type'))
            ->setSettings(['max_length' => 512, 'text_processing' => 0,]);

        $fields['entity_bundle'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Entity Bundle'))
            ->setSettings(['max_length' => 512, 'text_processing' => 0,]);

        $fields['context'] = BaseFieldDefinition::create('map')
            ->setLabel(t('Context'));

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'));

        return $fields;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->get('name')->value;
    }

    /**
     * @param string $name
     * @return EntityAction
     */
    public function setName(string $name): EntityAction
    {
        $this->set('name', $name);

        return $this;
    }

    /**
     * @return string
     */
    public function getActionType(): string
    {
        return $this->get('action_type')->value;
    }

    /**
     * @param string $actionType
     * @return EntityAction
     */
    public function setActionType(string $actionType): EntityAction
    {
        $this->set('action_type', $actionType);

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityEntityType(): string
    {
        return $this->get('entity_type')->value;
    }

    /**
     * @param string $entityType
     * @return EntityAction
     */
    public function setEntityEntityType(string $entityType): EntityAction
    {
        $this->set('entity_type', $entityType);

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityBundle(): string
    {
        return $this->get('entity_bundle')->value;
    }

    /**
     * @param string $entityBundle
     * @return EntityAction
     */
    public function setEntityBundle(string $entityBundle): EntityAction
    {
        $this->set('entity_bundle', $entityBundle);

        return $this;
    }

    /**
     * @return array
     * @throws MissingDataException
     */
    public function getContext(): array
    {
        return $this->get('context')->first()->getValue();
    }

    /**
     * @param array $context
     * @return EntityAction
     */
    public function setContext(array $context): EntityAction
    {
        $this->set('context', $context);

        return $this;
    }
}
