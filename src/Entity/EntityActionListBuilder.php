<?php

namespace Drupal\entity_action\Entity;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\TypedData\Exception\MissingDataException;

class EntityActionListBuilder extends EntityListBuilder
{
    public function buildHeader()
    {
        $header['id'] = $this->t('ID');
        $header['name'] = $this->t('Name');
        $header['action_type'] = $this->t('Action');
        $header['entity_type'] = $this->t('Entity Type');
        $header['entity_bundle'] = $this->t('Entity Bundle');
        $header['context'] = $this->t('Context');

        return $header + parent::buildHeader();
    }

    public function buildRow(EntityInterface $entity)
    {
        /* @var $entity EntityAction */

        $row['id'] = $entity->id();
        $row['name'] = $entity->get('name')->value;
        $row['action_type'] = $entity->get('action_type')->value;
        $row['entity_type'] = $entity->get('entity_type')->value;
        $row['entity_bundle'] = $entity->get('entity_bundle')->value;
        try {
            $row['context'] = json_encode($entity->get('context')->first()->getValue());
        }
        catch (MissingDataException $e) {
            Drupal::logger('entity_action')->error($e->getMessage());
        }

        return $row + parent::buildRow($entity);
    }
}
