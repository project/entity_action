<?php

namespace Drupal\entity_action\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class EntityActionAccessControlHandler extends EntityAccessControlHandler
{
    /**
     * {@inheritdoc}
     */
    protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
    {
        if ($account->hasPermission('administer entity action') || $account->id() === 1) {
            return AccessResult::allowedIfHasPermission($account, 'administer entity action');
        }

        switch ($operation) {
            case 'view':
                return AccessResult::allowedIfHasPermission($account, 'view entity action');

            case 'edit':
                return AccessResult::allowedIfHasPermission($account, 'edit entity action');

            case 'delete':
                return AccessResult::allowedIfHasPermission($account, 'delete entity action');
        }

        return AccessResult::allowed();
    }

    /**
     * {@inheritdoc}
     */
    protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = null)
    {
        if ($account->hasPermission('administer entity action') || $account->id() === 1) {
            return AccessResult::allowedIfHasPermission($account, 'administer entity action');
        }

        return AccessResult::allowedIfHasPermission($account, 'add entity action');
    }
}
