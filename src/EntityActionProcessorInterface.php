<?php

namespace Drupal\entity_action;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_action\Entity\EntityAction;

interface EntityActionProcessorInterface extends PluginInspectionInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getActionType(): string;

    /**
     * @return string
     */
    public function getEntityType(): string;

    /**
     * @return string
     */
    public function getEntityBundle(): string;

    /**
     * @param EntityInterface $entity
     * @param EntityAction $entityAction
     * @param bool $delete
     */
    static public function process(EntityInterface $entity, EntityAction $entityAction, bool &$delete): void;
}
