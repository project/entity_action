<?php

namespace Drupal\entity_action\Services;

use Drupal;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_action\EntityActionProcessorBase;
use Drupal\entity_action\Entity\EntityAction;
use Exception;

class EntityActionManagerService
{
    /**
     * @var EntityTypeManagerInterface
     */
    private $entityTypeManager;

    /**
     * @var PluginManagerInterface
     */
    private $pluginManager;

    /**
     * @var EntityAction[]
     */
    private $entityActions;

    /**
     * @var EntityActionProcessorBase[]
     */
    private $entityActionProcessors;

    public function __construct(EntityTypeManagerInterface $entityTypeManager, PluginManagerInterface $pluginManager)
    {
        $this->entityTypeManager = $entityTypeManager;
        $this->pluginManager = $pluginManager;
    }

    /**
     * @return EntityAction[]
     */
    protected function getEntityActions(): array
    {
        return $this->entityActions;
    }

    /**
     * @param EntityAction[] $entityActions
     */
    protected function setEntityActions(array $entityActions): void
    {
        $this->entityActions = $entityActions;
    }

    /**
     * @return EntityActionProcessorBase[]
     */
    protected function getEntityActionProcessors(): array
    {
        return $this->entityActionProcessors;
    }

    /**
     * @param EntityActionProcessorBase[] $entityActionProcessors
     */
    protected function setEntityActionProcessors(array $entityActionProcessors): void
    {
        $this->entityActionProcessors = $entityActionProcessors;
    }

    protected function fetchEntityActions(string $actionType, EntityInterface $entity)
    {
        $entityActions = [];

        try {
            $actions = $this->entityTypeManager->getStorage('entity_action')->loadByProperties(
                [
                    'action_type' => $actionType,
                    'entity_type' => $entity->getEntityTypeId(),
                    'entity_bundle' => $entity->bundle(),
                ]
            );

            if (count($actions)) {
                foreach ($actions as $entityAction) {
                    if ($entityAction instanceof EntityAction) {
                        $entityActions[] = $entityAction;
                    }
                }
            }
        }
        catch (Exception $e) {
            Drupal::logger('entity_action')->error($e->getMessage());
        }

        $this->setEntityActions($entityActions);
    }

    protected function fetchEntityActionProcessors(string $actionType, EntityInterface $entity)
    {
        $entityActionProcessors = [];

        $pluginDefinitions = $this->pluginManager->getDefinitions();
        if (count($pluginDefinitions)) {
            foreach ($pluginDefinitions as $pluginDefinition) {
                if (
                    $pluginDefinition['action_type'] === $actionType &&
                    $pluginDefinition['entity_type'] === $entity->getEntityTypeId() &&
                    $pluginDefinition['entity_bundle'] === $entity->bundle()
                ) {
                    try {
                        $entityActionProcessors[] = $this->pluginManager->createInstance($pluginDefinition['id'], $pluginDefinition);
                    }
                    catch (Exception $e) {
                        Drupal::logger('entity_action')->error($e->getMessage());
                    }
                }
            }
        }

        $this->setEntityActionProcessors($entityActionProcessors);
    }

    protected function processEntityWithEntityActions(EntityInterface $entity)
    {
        $entityActions = $this->getEntityActions();
        if (!count($entityActions)) {
            return;
        }

        $processors = $this->getEntityActionProcessors();
        if (!count($processors)) {
            return;
        }

        foreach ($entityActions as $entityAction) {
            try {
                $delete = false;
                foreach ($processors as $processor) {
                    $processor->process($entity, $entityAction, $delete);
                }
                if ($delete) {
                    $entityAction->delete();
                }
            }
            catch (Exception $e) {
                Drupal::logger('entity_action')->error($e->getMessage());
            }
        }
    }

    public function processActions(string $actionType, EntityInterface $entity)
    {
        $this->fetchEntityActions($actionType, $entity);
        $this->fetchEntityActionProcessors($actionType, $entity);
        $this->processEntityWithEntityActions($entity);
    }

    static public function createAction(string $name, string $actionType, string $entityType, string $entityBundle, array $context)
    {
        $entityAction = EntityAction::create(
            [
                'name' => $name,
                'action_type' => $actionType,
                'entity_type' => $entityType,
                'entity_bundle' => $entityBundle,
                'context' => $context,
            ]
        );
        try {
            $entityAction->save();
        }
        catch (Exception $e) {
            Drupal::logger('entity_action')->error($e->getMessage());
        }
    }
}
