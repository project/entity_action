<?php

namespace Drupal\entity_action\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

class EntityActionProcessorPluginManagerService extends DefaultPluginManager
{
    /**
     * EntityActionProcessorPluginManagerService constructor.
     * @param Traversable $namespaces
     * @param CacheBackendInterface $cache_backend
     * @param ModuleHandlerInterface $module_handler
     */
    public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
    {
        parent::__construct(
            'Plugin/entity_action/EntityActionProcessor',
            $namespaces,
            $module_handler,
            'Drupal\entity_action\EntityActionProcessorInterface',
            'Drupal\entity_action\Annotation\EntityActionProcessor'
        );
        $this->alterInfo('entity_action_processor_info');
        $this->setCacheBackend($cache_backend, 'entity_action_processor');
    }
}
