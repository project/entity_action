<?php

namespace Drupal\entity_action\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Annotation
 */
class EntityActionProcessor extends Plugin
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $action_type;

    /**
     * @var string
     */
    public $entity_type;

    /**
     * @var string
     */
    public $entity_bundle;

    /**
     * @var Translation
     *
     * @ingroup plugin_translatable
     */
    public $name;
}
